package com.paowaric.week12;

import java.util.LinkedList;
import java.util.Scanner;

public class SimpleQueueApp {
    private static Scanner sc = new Scanner(System.in);
    private static int menu=0;
    private static LinkedList<String> queue = new LinkedList();
    private static String current;
    public static void main(String[] args) {
        while(true) {
            showQueue();
            printMenu();
            inputMenu();
        }
 
    }

    private static void showQueue() {
        System.out.println(queue);
    }

    private static void inputMenu() {
        try {
        System.out.print("Please input menu (1-3) : ");
        menu = sc.nextInt();
        switch(menu) {
            case 1:
                newQueue();
                break;
            case 2:
                getQueue();
                break;
            case 3:
                exit();
                break;
            default:
                System.out.println("Error : Can only input 1 - 3");
            }
        } catch(Exception e) {
            System.out.println("Error : Please in put 1 - 3");
            sc.next();
        }       
    }

    private static void exit() {
        System.out.println("Thank You");
        System.exit(0);
    }

    private static void getQueue() {
        if(queue.isEmpty()) {
            System.out.println("Queue Empty");
            return;
        }
        current = queue.remove();
        System.out.println("Current: " + current);
    }

    private static void newQueue() {
        System.out.print("Please inpit your name : ");
        String name = sc.next();
        queue.add(name);
    }

    private static void printMenu() {
        System.out.println("------MENU------");
        System.out.println("New Queue prees 1");
        System.out.println("Get Queue prees 2");
        System.out.println("Exit prees 3");
        System.out.println("----------------");
    }
}
